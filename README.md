# Приложение "Со статьями авторов и комментариями"

**Описание:**

Это приложение представляет собой платформу для создания, редактирования и просмотра статей авторов, а также добавления комментариев к этим статьям.

**Примеры запросов для Graphql**

![изображение](docs/img.png)

![изображение](docs/img_1.png)

![изображение](docs/img_2.png)

![изображение](docs/img_3.png)

**Технические детали:**

- Язык программирования: Java 17
- Фреймворк: Spring Boot 2.1.5
- Библиотека для доступа к базе данных: Spring Data JPA
- Основной модуль для веб-разработки: Spring Web
- Инструменты разработки: Spring DevTools
- Контейнеризация: Docker Compose
- Система управления базой данных: MariaDB
- Библиотека для удобной разработки: Lombok
- Тестирование: Spring Starter Test
- Поддержка реактивного программирования: Spring WebFlux
- Поддержка запросов GraphQL: Spring GraphQL
- Система управления зависимостями: Apache Maven

**Зависимости:**

- Java 17: https://openjdk.java.net/projects/jdk/17/
- Spring Boot 2.1.5: https://spring.io/projects/spring-boot
- Spring Data JPA: https://spring.io/projects/spring-data-jpa
- Spring Web: https://spring.io/projects/spring-web
- Spring DevTools: https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot-devtools
- Docker Compose: https://docs.docker.com/compose/
- MariaDB: https://mariadb.org/
- Lombok: https://projectlombok.org/
- Spring Starter Test: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html
- Spring WebFlux: https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html
- Spring GraphQL: https://docs.spring.io/spring-graphql/docs/current/reference/html/
- Apache Maven: https://maven.apache.org/

**Инструкции по развертыванию:**

1. Установите Java 17.
2. Установите Docker Compose.
3. Установите MariaDB или запустите ее через Docker Compose.
4. Склонируйте репозиторий с приложением.
5. Скомпилируйте и установите зависимости с помощью Maven: `mvn clean install`.
6. Запустите приложение: `mvn spring-boot:run`.

**Участники:**

- Semenov Vladislav -> ooovladislavchik@gmail.com