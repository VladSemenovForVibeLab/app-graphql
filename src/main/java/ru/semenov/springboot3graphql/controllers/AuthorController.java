package ru.semenov.springboot3graphql.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.springboot3graphql.models.Author;
import ru.semenov.springboot3graphql.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorRepository authorRepository;
    @QueryMapping
    public Iterable<Author> authors(){
        return authorRepository.findAll();
    }
}
