package ru.semenov.springboot3graphql.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.springboot3graphql.models.Article;
import ru.semenov.springboot3graphql.models.ArticleInput;
import ru.semenov.springboot3graphql.models.Author;
import ru.semenov.springboot3graphql.repositories.ArticleRepository;
import ru.semenov.springboot3graphql.repositories.AuthorRepository;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/articles")
public class ArticleController {
    private final ArticleRepository articleRepository;
    private final AuthorRepository authorRepository;

    @QueryMapping
    public Iterable<Article> articles() {
        return articleRepository.findAll();
    }

    @MutationMapping
    public Article createArticle(@Argument  String title, @Argument String content, @Argument Long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(()->new IllegalArgumentException("Author not found"));
        Article article = Article
                .builder()
                .title(title)
                .content(content)
                .author(author)
                .build();
        return articleRepository.save(article);
    }
    @MutationMapping
    public Article addArticle(@Argument(name = "article")ArticleInput articleInput){
        Author author = authorRepository.findById(articleInput.getAuthorId())
                .orElseThrow(()->new IllegalArgumentException("Author not found"));
        Article article = Article
                .builder()
                .title(articleInput.getTitle())
                .content(articleInput.getContent())
                .author(author)
                .build();
        return articleRepository.save(article);
    }
    @MutationMapping
    public Article updateArticle(@Argument Long id,@Argument(name = "article") ArticleInput articleInput){
        Article article = articleRepository.findById(id)
                .orElseThrow(()->new IllegalArgumentException("Article not found!"));
        article.setTitle(articleInput.getTitle());
        article.setContent(articleInput.getContent());
        return articleRepository.save(article);
    }
    @MutationMapping
    public void deleteArticle(@Argument Long id){
        articleRepository.deleteById(id);
    }
}
