package ru.semenov.springboot3graphql.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    @Id
    @GeneratedValue
    Long id;
    String firstName;
    String lastName;
    @OneToMany(mappedBy = "author",cascade = CascadeType.ALL)
    List<Article> articles = new ArrayList<>();
}
