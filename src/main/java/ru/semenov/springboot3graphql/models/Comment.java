package ru.semenov.springboot3graphql.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    @Id
    @GeneratedValue
    Long id;
    String message;
    @ManyToOne(fetch = FetchType.LAZY)
    Article article;
}
