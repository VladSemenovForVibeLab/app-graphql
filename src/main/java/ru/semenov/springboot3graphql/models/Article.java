package ru.semenov.springboot3graphql.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    @Id
    @GeneratedValue
    Long id;
    String title;
    String content;
    @ManyToOne(fetch = FetchType.LAZY)
    Author author;
    @OneToMany(mappedBy = "article",cascade = CascadeType.ALL)
    List<Comment> comments = new ArrayList<>();
}
