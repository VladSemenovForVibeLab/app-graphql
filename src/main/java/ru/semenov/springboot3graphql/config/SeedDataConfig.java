package ru.semenov.springboot3graphql.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.semenov.springboot3graphql.models.Article;
import ru.semenov.springboot3graphql.models.Author;
import ru.semenov.springboot3graphql.models.Comment;
import ru.semenov.springboot3graphql.repositories.ArticleRepository;
import ru.semenov.springboot3graphql.repositories.AuthorRepository;
import ru.semenov.springboot3graphql.repositories.CommentRepository;

@Component
@RequiredArgsConstructor
public class SeedDataConfig implements CommandLineRunner {
    private final AuthorRepository authorRepository;
    private final ArticleRepository articleRepository;
    private final CommentRepository commentRepository;
    @Override
    public void run(String... args) throws Exception {
        if(authorRepository.count()==0){
            Author author = Author
                    .builder()
                    .firstName("Vlad")
                    .lastName("Semenov")
                    .build();
            author=authorRepository.save(author);
            Article article = Article
                    .builder()
                    .title("Newspaper")
                    .content("Many content")
                    .author(author)
                    .build();
            Article article1 = Article
                    .builder()
                    .title("Cats")
                    .content("Very beautiful")
                    .author(author)
                    .build();
            articleRepository.save(article);
            articleRepository.save(article1);
            Comment comment = Comment
                    .builder()
                    .message("Great!")
                    .article(article)
                    .build();
            Comment comment1 = Comment
                    .builder()
                    .message("Very beautiful!")
                    .article(article1)
                    .build();
            commentRepository.save(comment);
            commentRepository.save(comment1);
        }
    }
}
