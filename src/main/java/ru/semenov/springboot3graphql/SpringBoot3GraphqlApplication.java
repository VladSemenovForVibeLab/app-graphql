package ru.semenov.springboot3graphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot3GraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot3GraphqlApplication.class, args);
	}

}
