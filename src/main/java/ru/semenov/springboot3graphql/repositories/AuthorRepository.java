package ru.semenov.springboot3graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.springboot3graphql.models.Author;

public interface AuthorRepository extends JpaRepository<Author,Long> {

}
