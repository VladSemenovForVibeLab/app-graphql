package ru.semenov.springboot3graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.springboot3graphql.models.Article;

public interface ArticleRepository extends JpaRepository<Article,Long> {

}
