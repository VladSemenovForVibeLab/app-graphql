package ru.semenov.springboot3graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.springboot3graphql.models.Comment;

public interface CommentRepository extends JpaRepository<Comment,Long> {
}
